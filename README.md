# Amazon product upload
we need to install + configure java 8 (higher >=8) and maven to run this project
to setup java sdk: https://www3.ntu.edu.sg/home/ehchua/programming/howto/JDK_Howto.html
to setup maven: https://www.baeldung.com/install-maven-on-windows-linux-mac

run the command "mvn assembly:assembly -DdescriptorId=jar-with-dependencies"
to generate upload-1.1-jar-with-dependencies.jar file, that file will be in target folder

if you want to run upload-1.1-jar-with-dependencies.jar you have to copy this file and copy 2 other files
from resource "Canvas-Defender_v1.1.0.crx" and "Canvas-Fingerprint-Defender_v0.1.0.crx" into same directory
click on upload-1.1-jar-with-dependencies.jar to run project





