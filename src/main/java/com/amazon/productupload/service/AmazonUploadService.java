package com.amazon.productupload.service;

import com.amazon.productupload.client.ClientApi;
import com.amazon.productupload.constant.DataContants;
import com.amazon.productupload.driver.DriverFactory;
import com.amazon.productupload.model.Account;
import com.amazon.productupload.model.AsinProduct;
import com.amazon.productupload.model.Config;
import com.amazon.productupload.model.Product;
import com.amazon.productupload.model.Summary;
import com.amazon.productupload.util.FileUtil;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.amazon.productupload.constant.DataContants.ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY;
import static com.amazon.productupload.constant.DataContants.ADD_PRODUCT_ELEMENT;
import static com.amazon.productupload.constant.DataContants.ADD_PRODUCT_ERROR_MESSAGE;
import static com.amazon.productupload.constant.DataContants.BUILD_PRODUCT_ERROR_MESSAGE;
import static com.amazon.productupload.constant.DataContants.CALENADR_MONTH_ELEMENT;
import static com.amazon.productupload.constant.DataContants.CALENAR_NEXT_MONTH_BUTTON_ELEMENT;
import static com.amazon.productupload.constant.DataContants.CALENDAR_IMAGE_ELEMENT;
import static com.amazon.productupload.constant.DataContants.CALENDAR_PREVIOUS_MONTH_BUTTON_ELEMENT;
import static com.amazon.productupload.constant.DataContants.CALENDAR_YEAR_ELEMENT;
import static com.amazon.productupload.constant.DataContants.COLOR_CHECKBOX_ELEMENT;
import static com.amazon.productupload.constant.DataContants.CONFIRM_PUBLIC_BUTTON_ELEMENT;
import static com.amazon.productupload.constant.DataContants.CURRENT_CHROME_VERSION;
import static com.amazon.productupload.constant.DataContants.DASHBOARD_URL;
import static com.amazon.productupload.constant.DataContants.DATA_DRAFT_SHIRT_TYPE_NATIVE_ELEMENT;
import static com.amazon.productupload.constant.DataContants.DRAFT_RADIO_CHECKBOX_ELEMENT;
import static com.amazon.productupload.constant.DataContants.DROPDOWN_BUTTON_ELEMENT;
import static com.amazon.productupload.constant.DataContants.GENERATE_SUMMARY_BUTTON_ELEMENT;
import static com.amazon.productupload.constant.DataContants.GET_GET_PRODUCT_LIVE_PREFIX_URL;
import static com.amazon.productupload.constant.DataContants.GET_PRODUCT_LIVE_SUBFIX_URL;
import static com.amazon.productupload.constant.DataContants.HREF_ELEMENT;
import static com.amazon.productupload.constant.DataContants.ID;
import static com.amazon.productupload.constant.DataContants.INPUT_IMAGE_ELEMENT;
import static com.amazon.productupload.constant.DataContants.IS_MAXIMUM_PRODUCT_LIVE_ELEMENT;
import static com.amazon.productupload.constant.DataContants.IS_MEN_ELEMT;
import static com.amazon.productupload.constant.DataContants.IS_WOMEN_ELEMENT;
import static com.amazon.productupload.constant.DataContants.IS_YOUNG_ELEMENT;
import static com.amazon.productupload.constant.DataContants.LIMIT_ELEMENT;
import static com.amazon.productupload.constant.DataContants.LIMIT_PRODUCT_LIVE;
import static com.amazon.productupload.constant.DataContants.LIVE_NUMBER_ELEMENT;
import static com.amazon.productupload.constant.DataContants.LIVE_RADIO_CHECKBOX_ELEMENT;
import static com.amazon.productupload.constant.DataContants.LOGIN_URL_REFIX;
import static com.amazon.productupload.constant.DataContants.MANAGE_PRODUCT_URL;
import static com.amazon.productupload.constant.DataContants.MAXIMUM_PRODUCT_LIVE;
import static com.amazon.productupload.constant.DataContants.NUMBER_OF_PRODUCT_SOLD;
import static com.amazon.productupload.constant.DataContants.PASSWORD_ELEMENT;
import static com.amazon.productupload.constant.DataContants.PRICE_ELEMENT;
import static com.amazon.productupload.constant.DataContants.PRODUCT_DESCRIPTION_ELEMENT;
import static com.amazon.productupload.constant.DataContants.PRODUCT_LIVE_ELEMENT;
import static com.amazon.productupload.constant.DataContants.PRODUCT_NAME_ELEMENT;
import static com.amazon.productupload.constant.DataContants.PRODUCT_SUMARY_KEY_FIRST_ELEMENT;
import static com.amazon.productupload.constant.DataContants.PRODUCT_SUMARY_KEY_SECOND_ELEMENT;
import static com.amazon.productupload.constant.DataContants.PRODUCT_TABLE_ELEMENT;
import static com.amazon.productupload.constant.DataContants.PRODUCT_TITLE_ELEMENT;
import static com.amazon.productupload.constant.DataContants.PUBLISH_BUTTON_ELEMENT;
import static com.amazon.productupload.constant.DataContants.ROW_ELEMENT;
import static com.amazon.productupload.constant.DataContants.ROW_OF_PRODUCTS_ELEMENT;
import static com.amazon.productupload.constant.DataContants.ROYALTY_CHECKBOX_ELEMENT;
import static com.amazon.productupload.constant.DataContants.SAVE_AND_FINISH_BUILD_BUTTON_ELEMENT;
import static com.amazon.productupload.constant.DataContants.SAVE_AND_FINISH_UPLOAD_BUTTON_ELEMENT;
import static com.amazon.productupload.constant.DataContants.SEND_KEY_ERROR_MESSAGE;
import static com.amazon.productupload.constant.DataContants.SHIRT_TYPE_ELEMENT;
import static com.amazon.productupload.constant.DataContants.SKIP_UPLOAD_PRODUCT_ERROR_MESSAGE;
import static com.amazon.productupload.constant.DataContants.STATUS_UPLOAD_SUCCESS_ELEMENT;
import static com.amazon.productupload.constant.DataContants.SUBMIT_ELEMENT;
import static com.amazon.productupload.constant.DataContants.TIER_ELEMENT;
import static com.amazon.productupload.constant.DataContants.TOTAL_PURCHARSED_ELEMENT;
import static com.amazon.productupload.constant.DataContants.UPLOAD_IMAGE_BUTTON_ELEMENT;
import static com.amazon.productupload.constant.DataContants.ZERO_LIMIT;
import static com.amazon.productupload.constant.DataContants.ZERO_STRING;
import static com.amazon.productupload.constant.DataContants.pageSize;

public class AmazonUploadService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AmazonUploadService.class);
    private static final AmazonUploadService instance = new AmazonUploadService();

    private ClientApi clientApi = ClientApi.getInstance();
    private FileUtil fileUtil = FileUtil.getInstance();

    private WebDriverWait wait;
    private WebDriver driver;

    private AmazonUploadService() {
    }

    public static AmazonUploadService getInstance() {
        return instance;
    }

    private List<Product> getProductsFromServerByAccount(Account account) {
        return clientApi.getProductsByAccount(account);
    }

    private void updateProductLimitByAccount(Account account) throws IOException {
        setAmazonConfigToAccount(account);
        clientApi.updateProductLimitByAccount(account);
    }

    public void getSummaryByAccount(Account account) throws Exception {
        List<Summary> summaries = new ArrayList<>();
        try {
            driver = login(account, DataContants.ANALYZE_URL);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CALENDAR_IMAGE_ELEMENT)));
            driver.findElement(By.xpath(CALENDAR_IMAGE_ELEMENT)).click();

            //get month and year on calendar
            int numberYearFromCalendar = Integer.parseInt(driver.findElement(By.cssSelector(CALENDAR_YEAR_ELEMENT)).getText());
            int currentMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            int today = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            int lastDayOfPreviousMonth = LocalDate.now().withDayOfMonth(1).minusDays(1).getDayOfMonth();

            //move calendar to current year
            while (numberYearFromCalendar < currentYear) {
                driver.findElement(By.cssSelector(CALENAR_NEXT_MONTH_BUTTON_ELEMENT)).click();
                numberYearFromCalendar = Integer.parseInt(driver.findElement(By.cssSelector(CALENDAR_YEAR_ELEMENT)).getText());
            }

            String textMonthFromCalendar = driver.findElement(By.cssSelector(CALENADR_MONTH_ELEMENT)).getText();
            int numberMonthFromCalendar = DateTimeFormatter.ofPattern("MMMM").withLocale(Locale.ENGLISH).parse(textMonthFromCalendar).get(ChronoField.MONTH_OF_YEAR);

            //move calendar to current month
            while (numberMonthFromCalendar < currentMonth) {
                driver.findElement(By.cssSelector(CALENAR_NEXT_MONTH_BUTTON_ELEMENT)).click();
                textMonthFromCalendar = driver.findElement(By.cssSelector(CALENADR_MONTH_ELEMENT)).getText();
                numberMonthFromCalendar = DateTimeFormatter.ofPattern("MMMM").withLocale(Locale.ENGLISH).parse(textMonthFromCalendar).get(ChronoField.MONTH_OF_YEAR);
            }

            if (today != 1) {
                driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/table/tbody//td[.='" + (today - 1) + "']")).click();
            } else {
                driver.findElement(By.cssSelector(CALENDAR_PREVIOUS_MONTH_BUTTON_ELEMENT)).click();
                driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/table/tbody//td[.='" + lastDayOfPreviousMonth + "']")).click();
            }

            //click on go Button to generate summary data
            driver.findElement(By.xpath(GENERATE_SUMMARY_BUTTON_ELEMENT)).click();

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TOTAL_PURCHARSED_ELEMENT)));
            int purcharsed = Integer.parseInt(driver.findElement(By.xpath(TOTAL_PURCHARSED_ELEMENT)).getText());
            if (purcharsed == 0) {
                LOGGER.info("No product is sold account:{}", account.toString());
                updateProductLimitByAccount(account);
                exitDriver();
                return;
            }

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(NUMBER_OF_PRODUCT_SOLD)));
            int numberOfProducts = driver.findElement(By.cssSelector(NUMBER_OF_PRODUCT_SOLD)).getText().split("\n").length - 1;

            for (int i = 2; i < numberOfProducts + 2; i++) {
                String productInfor = driver.findElement(By.xpath("//*[@id=\"app_container\"]/div/div/div[7]/table/tbody/tr[" + i + "]")).getText();
                String asin = driver.findElement(By.xpath("//*[@id=\"app_container\"]/div/div/div[7]/table/tbody/tr[" + i + "]/td[1]")).findElement(By.cssSelector("span a")).getAttribute("href").substring(26);
                summaries.add(fileUtil.toProductSumary(account.getEmail(), productInfor, asin));
            }

            clientApi.postProductSaleSumary(account, summaries);
            updateProductLimitByAccount(account);
            exitDriver();
        } catch (Exception e) {
            driver.quit();
            driver = null;
            String errorMessage = String.format("Error when get summary for account: %s", account.getLabel());
            LOGGER.error(errorMessage, e.fillInStackTrace());
            clientApi.sendError(errorMessage, "empty", account, "empty");
            throw e;
        }
    }

    public void getProductAsinFromAmazonByAccount(Account account) throws InterruptedException, IOException {
        login(account, DataContants.MANAGE_URL);
        try {
            wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(By.xpath(LIVE_NUMBER_ELEMENT))));
            int live = Integer.parseInt(driver.findElement(By.xpath(LIVE_NUMBER_ELEMENT)).getText().split(" ")[0]);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"app_container\"]/div/div/div/div[5]/div[3]/span/span/span/span")));

            if (live == 0) {
                driver.quit();
                return;
            }

            int pageNumber = 1;
            int total = 0;
            do {
                getProductAsinFromAmazonByPath(pageNumber, pageSize, account);
                total = total + pageSize;
                pageNumber = pageNumber + 1;
            } while (total < live);

            updateProductLimitByAccount(account);
            exitDriver();
        } catch (Exception e) {
            driver.quit();
            String errorMessage = String.format("Error when get ProductAsinFromAmazonByAccount for account: %s", account.getLabel());
            LOGGER.error(errorMessage, e.fillInStackTrace());
            clientApi.sendError(errorMessage, "empty", account, "getProductAsinFromAmazonByAccount");
            throw e;
        }
    }

    private void getProductAsinFromAmazonByPath(int top, int skip, Account account) {
        List<AsinProduct> asinProducts = new ArrayList<>();
        driver.get(String.format(GET_GET_PRODUCT_LIVE_PREFIX_URL, top, skip, GET_PRODUCT_LIVE_SUBFIX_URL));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(PRODUCT_TABLE_ELEMENT)));
        List<WebElement> elements = driver.findElements(By.cssSelector(ROW_OF_PRODUCTS_ELEMENT));
        for (WebElement element : elements) {
            String asin = element.findElements(By.cssSelector(ROW_ELEMENT)).get(0).findElement(By.cssSelector("a")).getAttribute(HREF_ELEMENT).substring(26);
            String id = element.findElements(By.cssSelector(ROW_ELEMENT)).get(5).findElement(By.cssSelector(DROPDOWN_BUTTON_ELEMENT)).getAttribute(ID).split("_")[1];
            asinProducts.add(new AsinProduct(id, asin));
        }
        clientApi.postProductAsin(account, asinProducts);
    }

    public void uploadAllProducsByAccount(Account account) throws Exception {
        login(account, DASHBOARD_URL);
        setAmazonConfigToAccount(account);
        //in the case all of products are live
        if (ZERO_STRING.equals(account.getConfig().getLimit())) {
            updateProductLimitByAccount(account);
            exitDriver();
            return;
        }

        List<Product> products = getProductsFromServerByAccount(account);
        if (products.isEmpty()) {
            updateProductLimitByAccount(account);
            exitDriver();
            LOGGER.error("Upload product-> there are no any product can get from server for account:{}", account.getLabel());
            return;
        }

        for (Product product : products) {
            if (product.isUpload()) {
                continue;
            }

            try {
                uploadProductByAccount(account, product);
                fileUtil.deleteFile(product.getUrl_image());
            } catch (Exception e) {
                exitDriver();
                throw e;
            }
        }
        //update product limit after upload all products for account
        updateProductLimitByAccount(account);
        exitDriver();
    }


    private void downloadImageByProduct(Account account, Product product) throws Exception {
        String localImageUrl = "";

        if (product.getUrl_image() == null || product.getUrl_image().isEmpty()) {
            String errorMessage = "Error when download image, image is :" + product.getUrl_image();
            clientApi.sendError(errorMessage, product.getId(), account, product.getTitle());
            LOGGER.error(ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, errorMessage, account.toString(), product.toString());
            exitDriver();
            throw new Exception("IMAGE URL is null");
        }

        try {
            localImageUrl = fileUtil.saveImage(product.getUrl_image());
            product.setUrl_image(localImageUrl);
        } catch (Exception e) {
            try {
                //try to download one more time
                LOGGER.info("DEBUG infor try to download image one more time url {} account:{}, productInfo: {}", product.getUrl_image(), account.toString(), product.toString(), e.fillInStackTrace());
                localImageUrl = fileUtil.saveImage(product.getUrl_image());
                product.setUrl_image(localImageUrl);
            } catch (Exception e1) {
                String errorMessage = "Error when download image, localImageUrl: " + localImageUrl;
                clientApi.sendError(errorMessage, product.getId(), account, product.getTitle());
                LOGGER.error(ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, errorMessage, account.toString(), product.toString(), e1.fillInStackTrace());
                driver.quit();
                driver = null;
                throw e1;
            }
        }
    }


    public void uploadProductByAccount(Account account, Product product) throws InterruptedException {
        //step 1 : click on add product button
        try {
            addProduct(account, product, 0);

            //step2: upload image, choose product type, marketplace
            uploadProduct(account, product, 0);

            //step3: choose fit type (men, women, youth) , colors, price
            buildProduct(account, product, 0);

            //step4: add all other information regarding product (name, title, desciption...)
            addDetail(account, product, 0);

            //step5: check all informations and submit product to live
            appReview(account, product,0 );

            //last step: make sure product is uploaded to amazon and send status to server
            sendStatus(account, product);
        } catch (Exception e) {
            LOGGER.error(SKIP_UPLOAD_PRODUCT_ERROR_MESSAGE, e.fillInStackTrace());
        }
    }


    private void addProduct(Account account, Product product, int retry) throws Exception {
        try {
            driver.get(DASHBOARD_URL);
            downloadImageByProduct(account, product);

            LOGGER.info("Start addProduct account:{}", account.getEmail(), product.toString());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(ADD_PRODUCT_ELEMENT)));
            LOGGER.info("Start addProduct -> click on addProduct button account:{} {}", account.getLabel(), product.getId());
            driver.findElement(By.xpath(ADD_PRODUCT_ELEMENT)).click();
        } catch (Exception e) {
            if (retry < 3) {
                LOGGER.warn(ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, SEND_KEY_ERROR_MESSAGE, account.toString(), product.toString(), e.fillInStackTrace());
                LOGGER.warn("Retry to addProduct, time {}", retry);
                driver.get(driver.getCurrentUrl());
                addProduct(account, product, retry + 1);
            } else {
                clientApi.sendError(ADD_PRODUCT_ERROR_MESSAGE, product.getId(), account, product.getTitle());
                LOGGER.error(ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, ADD_PRODUCT_ERROR_MESSAGE, account.toString(), product.toString(), e.fillInStackTrace());
                throw e;
            }
        }
    }

    private void uploadProduct(Account account, Product product, int retry) throws InterruptedException {
        LOGGER.info("Start uploadProduct");
        if (product.getProductype() > 4) {
            product.setProductype(4);
        }

        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.id(UPLOAD_IMAGE_BUTTON_ELEMENT)));
            driver.findElement(By.id(INPUT_IMAGE_ELEMENT)).sendKeys(product.getUrl_image());
//            Thread.sleep(20000);
            if (product.getProductype() != 0) {
                wait.until(ExpectedConditions.elementToBeClickable(By.id(SHIRT_TYPE_ELEMENT)));
                driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
                driver.findElement(By.id(SHIRT_TYPE_ELEMENT)).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.id(DATA_DRAFT_SHIRT_TYPE_NATIVE_ELEMENT)));
                driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
                driver.findElement(By.id(DATA_DRAFT_SHIRT_TYPE_NATIVE_ELEMENT + product.getProductype())).click();
            }

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            WebElement element = driver.findElement(By.id(SAVE_AND_FINISH_UPLOAD_BUTTON_ELEMENT));
            ExpectedCondition<Boolean> elementIsDisplayed = new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver arg0) {
                    try {
                        return !element.isEnabled();
                    } catch (Exception e) {
                        return false;
                    }
                }
            };

            wait.until(elementIsDisplayed);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            Thread.sleep(14000);

//            for just support DE markePlace
//            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Choose marketplace:'])[1]/following::span[5]")));
//            driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Choose marketplace:'])[1]/following::span[5]")).click();
//            driver.findElement(By.xpath("data-marketplace-native_2")).click();

//            for UK market
//            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Choose marketplace:'])[1]/following::span[5]")));
//            driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Choose marketplace:'])[1]/following::span[5]")).click();
//            driver.findElement(By.xpath("data-marketplace-native_2")).click();
            LOGGER.info("Start uploadProduct-> click on save-and-continue-upload-art-announce");
            element.click();
        } catch (Exception e) {
            if (retry < 3) {
                LOGGER.warn(ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, SEND_KEY_ERROR_MESSAGE, account.toString(), product.toString(), e.fillInStackTrace());
                LOGGER.warn("Retry to uploadProduct, time {}", retry);
                driver.get(driver.getCurrentUrl());
                uploadProduct(account, product, retry + 1);
            } else {
                LOGGER.error(ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, SEND_KEY_ERROR_MESSAGE, account.toString(), product.toString(), e.fillInStackTrace());
                ClientApi.getInstance().sendError(SEND_KEY_ERROR_MESSAGE, product.getId(), account, product.getTitle());
                throw e;
            }
        }
    }

    private void buildProduct(Account account, Product product, int retry) throws InterruptedException {
        try {
            //choose sexual
            LOGGER.info("Start build product");
            if (product.getProductype() < 2) {
                LOGGER.info("build product -> product type <2 ");
                if (product.is_youth()) {
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IS_YOUNG_ELEMENT)));
                    driver.findElement(By.xpath(IS_YOUNG_ELEMENT)).click();
                }
                if (!product.is_men()) {
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IS_MEN_ELEMT)));
                    driver.findElement(By.xpath(IS_MEN_ELEMT)).click();
                }
                if (!product.is_women()) {
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IS_WOMEN_ELEMENT)));
                    driver.findElement(By.xpath(IS_WOMEN_ELEMENT)).click();
                }
            } else {
                LOGGER.info("build product -> product type >= 2 ");
                wait.until(ExpectedConditions.elementToBeClickable(By.id("gear-checkbox-heather_grey")));
            }

            //choose colors
            LOGGER.info(" build product -> choose color");
            String[] colors = product.getColor().split(";");
            Thread.sleep(5000);
            for (String color : colors) {
                Thread.sleep(800);
                String colorElement = COLOR_CHECKBOX_ELEMENT + color;
                try {
                    wait.until(ExpectedConditions.elementToBeClickable(By.id(colorElement)));
                    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
                    driver.findElement(By.id(colorElement)).click();
                } catch (Exception e) {
                    LOGGER.warn("Could not find colors {}", color);
                }
                if (product.getProductype() < 2) {
                    wait.until(ExpectedConditions.elementToBeClickable(By.id(ROYALTY_CHECKBOX_ELEMENT)));
                    driver.findElement(By.id(ROYALTY_CHECKBOX_ELEMENT)).click();
                }
            }

            //input price
            LOGGER.info(" build product -> input price");
            driver.findElement(By.id(PRICE_ELEMENT)).click();
            driver.findElement(By.id(PRICE_ELEMENT)).clear();
            driver.findElement(By.id(PRICE_ELEMENT)).sendKeys(product.getPrice());
            wait.until(ExpectedConditions.elementToBeClickable(By.id(SAVE_AND_FINISH_BUILD_BUTTON_ELEMENT)));
            driver.findElement(By.id(SAVE_AND_FINISH_BUILD_BUTTON_ELEMENT)).click();
        } catch (Exception e) {
            if (retry < 3) {
                LOGGER.warn(ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, BUILD_PRODUCT_ERROR_MESSAGE, account.toString(), product.toString(), e.getMessage());
                LOGGER.warn("Retry to build product time {}", retry);
                driver.get(driver.getCurrentUrl());
                buildProduct(account, product, retry + 1);
            } else {
                clientApi.sendError(BUILD_PRODUCT_ERROR_MESSAGE, product.getId(), account, product.getTitle());
                throw e;
            }
        }
    }


    private void sendStatus(Account account, Product product) {
        LOGGER.info("Start send status");
        driver.get(MANAGE_PRODUCT_URL);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(STATUS_UPLOAD_SUCCESS_ELEMENT)));
            String status = driver.findElement(By.xpath(STATUS_UPLOAD_SUCCESS_ELEMENT)).getText();
            clientApi.sendSucess(product.getId(), product.getMerch_amazon_key(), status, account);
        } catch (Exception e) {
            String errorMessage = "Error :Get status default";
            clientApi.sendError(errorMessage, product.getId(), account, product.getTitle());
            LOGGER.error(ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, errorMessage, account.toString(), product.toString(), e.getMessage());
        }
    }

    private String getMerchId(WebDriver driver) {
        String currentUrl = driver.getCurrentUrl();
        return currentUrl.split("/")[5];
    }

    private void addDetail(Account account, Product product, int retry) throws InterruptedException {
        LOGGER.info("Start add Detail account: {}, productId: {}", account.getLabel(), product.getId());
        product.setMerch_amazon_key(getMerchId(driver));
        String marketName = "us";
        String marketLanguage = "en";
//        if MARKETPLACE == 'UK' :
//        marketName = 'gb'
//        if MARKETPLACE == 'DE' :
//        marketName = 'de'
//        marketLanguage = 'de'
        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.id(PRODUCT_NAME_ELEMENT)));
            driver.findElement(By.id(PRODUCT_NAME_ELEMENT)).click();
            driver.findElement(By.id(PRODUCT_NAME_ELEMENT)).clear();
            driver.findElement(By.id(PRODUCT_NAME_ELEMENT)).sendKeys(product.getBrandname());

            driver.findElement(By.id(PRODUCT_TITLE_ELEMENT + marketLanguage + "-" + marketName)).click();
            driver.findElement(By.id(PRODUCT_TITLE_ELEMENT + marketLanguage + "-" + marketName)).clear();
            driver.findElement(By.id(PRODUCT_TITLE_ELEMENT + marketLanguage + "-" + marketName)).sendKeys(product.getTitle());

            driver.findElement(By.id(PRODUCT_SUMARY_KEY_FIRST_ELEMENT + marketLanguage + "-" + marketName)).click();
            driver.findElement(By.id(PRODUCT_SUMARY_KEY_FIRST_ELEMENT + marketLanguage + "-" + marketName)).clear();
            driver.findElement(By.id(PRODUCT_SUMARY_KEY_FIRST_ELEMENT + marketLanguage + "-" + marketName)).sendKeys(product.getKey_1());

            driver.findElement(By.id(PRODUCT_SUMARY_KEY_SECOND_ELEMENT + marketLanguage + "-" + marketName)).click();
            driver.findElement(By.id(PRODUCT_SUMARY_KEY_SECOND_ELEMENT + marketLanguage + "-" + marketName)).clear();
            driver.findElement(By.id(PRODUCT_SUMARY_KEY_SECOND_ELEMENT + marketLanguage + "-" + marketName)).sendKeys(product.getKey_2());

            driver.findElement(By.id(PRODUCT_DESCRIPTION_ELEMENT + marketLanguage + "-" + marketName)).click();
            driver.findElement(By.id(PRODUCT_DESCRIPTION_ELEMENT + marketLanguage + "-" + marketName)).clear();
            driver.findElement(By.id(PRODUCT_DESCRIPTION_ELEMENT + marketLanguage + "-" + marketName)).sendKeys(product.getDescription());
            LOGGER.info("Add Detail-> click on save-and-continue-announce button");
            driver.findElement(By.id("save-and-continue-announce")).click();
            Thread.sleep(5000);
        } catch (Exception e) {
            if (retry < 3) {
                LOGGER.warn("Error :Get status default" + ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, e.getMessage(), account.toString(), product.toString());
                LOGGER.warn("Retry to addDetail time {}", retry);
                driver.get(driver.getCurrentUrl());
                addDetail(account, product, retry+1);
            } else {
                LOGGER.error("Error :Get status default" + ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, e.getMessage(), account.toString(), product.toString());
                clientApi.sendError("Error : add details", product.getId(), account, product.getTitle());
                throw e;
            }
        }
    }

    private void appReview(Account account, Product product, int retry) throws InterruptedException {
        try {
            LOGGER.info("Start appReview, account: {}, productId:{}", account.getLabel(), product.getId());
            if ("live".equals(account.getReview())) {
                LOGGER.info("appReview -> review = live");
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath(LIVE_RADIO_CHECKBOX_ELEMENT)));
                wait.until(ExpectedConditions.elementToBeClickable(By.id(PUBLISH_BUTTON_ELEMENT)));
                driver.findElement(By.id(PUBLISH_BUTTON_ELEMENT)).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CONFIRM_PUBLIC_BUTTON_ELEMENT)));
                LOGGER.info("appReview -> click on confirm button");
                Thread.sleep(5000);
                driver.findElement(By.xpath(CONFIRM_PUBLIC_BUTTON_ELEMENT)).click();
            } else {
                LOGGER.info("appReview -> review not live");
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath(DRAFT_RADIO_CHECKBOX_ELEMENT)));
                driver.findElement(By.xpath(DRAFT_RADIO_CHECKBOX_ELEMENT)).click();
                LOGGER.info("appReview -> click on confirm button");
                driver.findElement(By.id("a-autoid-5-announce")).click();
            }
        } catch (Exception e) {
            if (retry < 3) {
                LOGGER.warn(ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, "Review Live", account.toString(), product.toString(), e.getMessage());
                LOGGER.warn("Retry to appReview, time {}", retry);
                driver.get(driver.getCurrentUrl());
                appReview(account, product, retry+1);
            } else {
                LOGGER.error(ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY, "Review Live", account.toString(), product.toString(), e.getMessage());
                clientApi.sendError("Error :review live", product.getId(), account, product.getTitle());
                throw e;
            }
        }
    }

    public void exitDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    public WebDriver login(Account account, String url) throws InterruptedException {
        driver = DriverFactory.getInstance().getDriver(account, CURRENT_CHROME_VERSION);
        try {
            driver.get(url);
            wait = new WebDriverWait(driver, 100);
            Thread.sleep(2000);
            driver.switchTo().window(driver.getWindowHandle());
            if (driver.getCurrentUrl().contains(LOGIN_URL_REFIX)) {
                wait.until(ExpectedConditions.elementToBeClickable(By.id(PASSWORD_ELEMENT)));
                driver.findElement(By.id(DataContants.EMAIL_ELEMENT)).sendKeys(account.getEmail());
                driver.findElement(By.id(DataContants.PASSWORD_ELEMENT)).sendKeys(account.getPassword());
                driver.findElement(By.id(SUBMIT_ELEMENT)).sendKeys(Keys.ENTER);
            }
            return driver;
        } catch (Exception e) {
            String errorMessage = String.format("Error when try to Login with url: %s, account: %s", url, account.getLabel());
            LOGGER.error(errorMessage, e.fillInStackTrace());
            clientApi.sendError(errorMessage, "empty", account, "Empty");
            throw e;
        }
    }

    public void checkAccount(Account account) throws InterruptedException, IOException {
        login(account, DASHBOARD_URL);
        updateProductLimitByAccount(account);
    }

    private void setAmazonConfigToAccount(Account account) {
        try {
            driver.get(DASHBOARD_URL);
            LOGGER.info("Start setAmazonConfigToAccount account: {}", account.getLabel());
            wait.until(ExpectedConditions.or(
                    ExpectedConditions.elementToBeClickable(By.xpath(IS_MAXIMUM_PRODUCT_LIVE_ELEMENT)),
                    ExpectedConditions.elementToBeClickable(By.xpath(LIMIT_ELEMENT)),
                    ExpectedConditions.elementToBeClickable(By.xpath(PRODUCT_LIVE_ELEMENT)),
                    ExpectedConditions.elementToBeClickable(By.xpath(TIER_ELEMENT))
            ));

            Config config = new Config();
            if (MAXIMUM_PRODUCT_LIVE.equals(driver.findElement(By.xpath(IS_MAXIMUM_PRODUCT_LIVE_ELEMENT)).getText()) ||
                    LIMIT_PRODUCT_LIVE.equals(driver.findElement(By.xpath(LIMIT_ELEMENT)).getText())) {
                config.setLimit(ZERO_STRING);
                config.setTier(driver.findElement(By.xpath(TIER_ELEMENT)).getText());
                config.setProductLive(config.getTier());
                account.setConfig(config);
                return;
            }

            String limitString = driver.findElement(By.xpath(LIMIT_ELEMENT)).getText();
            String productLive = driver.findElement(By.xpath(PRODUCT_LIVE_ELEMENT)).getText();
            String tier = driver.findElement(By.xpath(TIER_ELEMENT)).getText();
            int tierNumber = Integer.parseInt(tier);
            int productliveNumber = Integer.parseInt(productLive);
            if (ZERO_LIMIT.equals(limitString)) {
                limitString = ZERO_STRING;
            } else if (Integer.parseInt(limitString) > tierNumber - productliveNumber) {
                limitString = String.valueOf(tierNumber - productliveNumber);
            }
            config.setLimit(limitString);
            config.setProductLive(productLive);
            config.setTier(tier);
            account.setConfig(config);
        } catch (Exception e) {
            String errorMessage = String.format("Error :when call setAmazonConfigToAccount method account: %s", account.getLabel());
            clientApi.sendError(errorMessage, "Empty", account, "Empty");
            LOGGER.error(errorMessage, e.fillInStackTrace());
            throw e;
        }
    }
}
