package com.amazon.productupload.constant;

import java.awt.*;

public class DataContants {

    //common string
    public static final String CURRENT_CHROME_VERSION = "75";
    public static final String COMMA = ",";
    public static final String ZERO_LIMIT = "have reached the submission limit for the day.";
    public static final String MAXIMUM_PRODUCT_LIVE = "You have reached the maximum number of live products.";
    public static final String LIMIT_PRODUCT_LIVE = "have reached the submission limit for the day.";
    public static final String ZERO_STRING = "0";
    public static final String ACCOUNT_CACHE_FILE = "cachedAccount.csv";
    public static final Integer FONT_SIZE_DEFAULT = 18;

    public static final Font FONT_BOLD_DEFAULT = new Font(null, Font.BOLD, FONT_SIZE_DEFAULT);
    public static final Font FONT_BOLD_DEFAULT_16 = new Font(null, Font.BOLD, 16);
    public static final Font FONT_LEFT_TO_RIGHT_DEFAULT = new Font(null, Font.LAYOUT_LEFT_TO_RIGHT, FONT_SIZE_DEFAULT);

    //Parameters
    public static final String EMAIL_FIRST_PARAM = "?email=";
    public static final String STATUS_FIRST_PARAM = "?status=";
    public static final String KEY_PARAM = "&key=";
    public static final String AMAZON_KEY_PARAM = "&merch_amazon_key=";
    public static final String DETAIL_PARAM = "&detail=";
    public static final String TITLE_PARAM = "&title=";
    public static final String PRODUCT_ID_PARAM = "&products_id=";
    public static final String LIMIT_PARAM = "&limit=";
    public static final String TIER_PARAM = "&tier=";
    public static final String LIVE_PARAM = "&products_live=";

    //Error message
    public static final String ADD_PRODUCT_ERROR_MESSAGE = "Error :Add a product";
    public static final String BUILD_PRODUCT_ERROR_MESSAGE = "Error: Build a product";
    public static final String SEND_KEY_ERROR_MESSAGE = "Error : send keys upload product";
    public static final String ACCOUNT_AND_PRODUCT_INFOR_MESSAGE_KEY = "Error:{} account:{}, product:{} {}";
    public static final String SKIP_UPLOAD_PRODUCT_ERROR_MESSAGE = "Skip upload product because error";


//URLs
    //AMAZON URL
    public static final String PROXY_REFIX = "--proxy-server=http://";
    public static final String ACCOUNT_INFO_URL = "https://account-merch.amazon.com/account";
    public static final String DASHBOARD_URL = "https://merch.amazon.com/dashboard";
    public static final String ANALYZE_URL = "https://merch.amazon.com/analyze";
    public static final String MANAGE_URL = "https://merch.amazon.com/manage/products";
    public static final String LOGIN_URL_REFIX = "https://www.amazon.com/ap/signin";
    public static final String MANAGE_PRODUCT_URL = "https://merch.amazon.com/manage/products";
    public static final String GET_GET_PRODUCT_LIVE_PREFIX_URL = "https://merch.amazon.com/manage/products?pageNumber=%s&pageSize=%s%s";
    public static final String GET_PRODUCT_LIVE_SUBFIX_URL = "&statusFilters=%5B%22LIVE%22%5D&marketplaceId=ATVPDKIKX0DER";


    //uoymedia URL
    public static final String SERVER_URL = "https://uoymedia.dev/";
    public static final String GET_PRODUCTS_URL = SERVER_URL + "products/amazon";
    public static final String ERROR_URL = SERVER_URL + "error_history/amazon";
    public static final String SUCCESS_URL = SERVER_URL + "products/amazon/success";
    public static final String POST_PRODUCT_ASIN_INFO_URL = SERVER_URL + "api/product/asins";
    public static final String POST_PRODUCT_SUMMARY_URL = SERVER_URL + "api/sale/asins";
    public static final String UPDATE_PRODUCT_LIMIT_URL = SERVER_URL + "merches/amazon";



    public static final int pageSize = 100;

    //Web elements
    public static final String EMAIL_ELEMENT = "ap_email";
    public static final String PASSWORD_ELEMENT = "ap_password";
    public static final String SUBMIT_ELEMENT = "signInSubmit";
    public static final String LIMIT_ELEMENT = "//div[@class='a-alert-content']/div[1]/span[5]";
    public static final String IS_MAXIMUM_PRODUCT_LIVE_ELEMENT = "//div[@class='a-alert-content']/div[1]";
    public static final String IS_YOUNG_ELEMENT = "//div[@id='shirt-configurations-fit-type-youth-field']/div/span/div/label/i";
    public static final String IS_MEN_ELEMT = "//div[@id='shirt-configurations-fit-type-men-field']/div/span/div/label/i";
    public static final String IS_WOMEN_ELEMENT = "//div[@id='shirt-configurations-fit-type-women-field']/div/span/div/label/i";
    public static final String SHIRT_TYPE_ELEMENT = "data-draft-shirt-type";
    public static final String PRODUCT_LIVE_ELEMENT = "//div[@class='a-alert-content']/div[1]/span[2]";
    public static final String TIER_ELEMENT = "//div[@class='a-alert-content']/div[2]/span[2]/span[2]";
    public static final String ADD_PRODUCT_ELEMENT = "//a[text()=\"Add a product\"]";
    public static final String UPLOAD_IMAGE_BUTTON_ELEMENT = "data-draft-tshirt-assets-front-image-asset-cas-shirt-art-image-file-upload-browse-button-announce";
    public static final String INPUT_IMAGE_ELEMENT = "data-draft-tshirt-assets-front-image-asset-cas-shirt-art-image-file-upload-AjaxInput";
    public static final String PRICE_ELEMENT = "data-draft-list-prices-marketplace-amount";
    public static final String PRODUCT_NAME_ELEMENT = "data-draft-brand-name";
    public static final String PRODUCT_TITLE_ELEMENT = "data-draft-name-";
    public static final String PRODUCT_SUMARY_KEY_FIRST_ELEMENT = "data-draft-bullet-points-bullet1-";
    public static final String PRODUCT_SUMARY_KEY_SECOND_ELEMENT = "data-draft-bullet-points-bullet2-";
    public static final String PRODUCT_DESCRIPTION_ELEMENT = "data-draft-description-";
    public static final String SAVE_AND_FINISH_UPLOAD_BUTTON_ELEMENT = "save-and-continue-upload-art-announce";
    public static final String SAVE_AND_FINISH_BUILD_BUTTON_ELEMENT ="save-and-continue-choose-variations-announce";
    public static final String DATA_DRAFT_SHIRT_TYPE_NATIVE_ELEMENT = "data-draft-shirt-type-native_";
    public static final String COLOR_CHECKBOX_ELEMENT = "gear-checkbox-";
    public static final String ROYALTY_CHECKBOX_ELEMENT = "gear-tshirt-royalty";
    public static final String PUBLISH_BUTTON_ELEMENT = "publish-announce";
    public static final String CONFIRM_PUBLIC_BUTTON_ELEMENT = "//div[@id='a-popover-content-2']/div/div/span/span/span/button";
    public static final String LIVE_RADIO_CHECKBOX_ELEMENT = "//div[@id='data-shirt-configurations-is-discoverable-accordion']/div[3]/div/div/a/i";
    public static final String DRAFT_RADIO_CHECKBOX_ELEMENT = "//div[@id='data-shirt-configurations-is-discoverable-accordion']/div/div/div/a/i";
    public static final String STATUS_UPLOAD_SUCCESS_ELEMENT = "//*[@id=\"gear-manage-table\"]/tbody/tr[2]/td[5]/div";

    //get sale
    public static final String CALENDAR_IMAGE_ELEMENT = "//*[@id=\"app_container\"]/div/div/div[2]/div[2]/div[2]/div[1]/div/img";
    public static final String CALENDAR_YEAR_ELEMENT = "span.ui-datepicker-Year";
    public static final String CALENADR_MONTH_ELEMENT = "span.ui-datepicker-Month";
    public static final String CALENAR_NEXT_MONTH_BUTTON_ELEMENT = ".ui-datepicker-next";
    public static final String CALENDAR_PREVIOUS_MONTH_BUTTON_ELEMENT = ".ui-datepicker-prev";
    public static final String GENERATE_SUMMARY_BUTTON_ELEMENT = "//*[@id=\"app_container\"]/div/div/div[2]/div[2]/div[2]/div[5]/span/span";
    public static final String TOTAL_PURCHARSED_ELEMENT = "//*[@id=\"app_container\"]/div/div/div[3]/div[2]/div[2]/div[1]/div[2]";
    public static final String NUMBER_OF_PRODUCT_SOLD = "table.gear-manage-table";


    //GET asin
    public static final String LIVE_NUMBER_ELEMENT = "//*[@id=\"app_container\"]/div/div/div/div[1]/div/div/div/div[1]/span[2]";
    public static final String PRODUCT_TABLE_ELEMENT ="table#gear-manage-table";
    public static final String ROW_OF_PRODUCTS_ELEMENT = "table#gear-manage-table tr:nth-child(n+2)";
    public static final String ROW_ELEMENT = "td";
    public static final String HREF_ELEMENT = "href";
    public static final String ID = "id";
    public static final String DROPDOWN_BUTTON_ELEMENT = "span.a-button-dropdown";

    //UI APPLICATION LABEL
    public static final String ERROR_CSV_LABEL = "<html><h2>Please help to check csv file content that should has structure like example.</h3><br><br> email,pass,key,review,proxy,profile,label<br> myname@gmail.com,mypassword,$2a$08$q8LmfQGtcnjrTJCgP4i5OuX8wLxS4ZXsqpgj3kZbdbeu,live,yourIP:yourportNumber,96,omt45</html>";
    public static final String ERROR_CHECKLOGIN_LABEL = "<html>Can't login<br> Please check internet and account config of account: %s  </html>";
    public static final String SUCCESS_CHECKLOGIN_LABEL = "Finish Check Account process";
    public static final String SUCCESS_UPLOAD_LABEL = "Finish Upload Product process";
    public static final String SUCCESS_GET_SALE_LABEL = "Finish Get Sale";
    public static final String SUCCESS_GET_PRODUCT_LABEL = "Finish Get Products process";


    private DataContants() {
    }

    @Override
    public String toString() {
        return super.toString();
    }
}