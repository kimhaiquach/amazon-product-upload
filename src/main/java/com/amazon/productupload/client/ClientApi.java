package com.amazon.productupload.client;

import com.google.gson.Gson;

import com.amazon.productupload.model.Account;
import com.amazon.productupload.model.AsinProduct;
import com.amazon.productupload.model.AsinProductData;
import com.amazon.productupload.model.Product;
import com.amazon.productupload.model.Summary;
import com.amazon.productupload.model.SummaryData;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static com.amazon.productupload.constant.DataContants.*;

public class ClientApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientApi.class);
    private static final ClientApi intance = new ClientApi();
    CloseableHttpClient client;

    private ClientApi() {
    }

    public static final ClientApi getInstance() {
        return intance;
    }

    public List<Product> getProductsByAccount(Account account) {
        try {
            ObjectMapper parse = new ObjectMapper();
            String getProductUrl = GET_PRODUCTS_URL + EMAIL_FIRST_PARAM + account.getEmail() + KEY_PARAM + account.getKey()
                    + LIMIT_PARAM + account.getConfig().getLimit() + TIER_PARAM + account.getConfig().getTier();
            LOGGER.info("Get product from uoymedia service getProductUrl: {} by account: {} ", getProductUrl, account.toString());

            HttpPost post = new HttpPost(getProductUrl);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode == HttpStatus.SC_OK) {
                String result = EntityUtils.toString(response.getEntity());
                return parse.readValue(result, new TypeReference<List<Product>>() {
                });
            }

            LOGGER.error("Error to get product for account {}.", account.toString(), response.getStatusLine().getStatusCode());
            return Collections.emptyList();
        } catch (Exception e) {
            LOGGER.info("Error when get product from uoymedia service by account: {} ", account.toString());
            e.getStackTrace();
            return Collections.emptyList();
        }
    }

    public void updateProductLimitByAccount(Account account) throws IOException {
        try {
            String updateProductLimitUrl = UPDATE_PRODUCT_LIMIT_URL + EMAIL_FIRST_PARAM + account.getEmail() + KEY_PARAM + account.getKey()
                    + LIMIT_PARAM + account.getConfig().getLimit() + TIER_PARAM + account.getConfig().getTier() + LIVE_PARAM + account.getConfig().getProductLive();
            LOGGER.info("update product limit to uoymedia server updateLimitProductUrl: {} by account: {} ", updateProductLimitUrl, account.toString());

            HttpPost post = new HttpPost(updateProductLimitUrl);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode != HttpStatus.SC_OK) {
                LOGGER.error("Error to update product limit for account {}.", account.toString(), response.getStatusLine().getStatusCode());
            }
        } catch (Exception e) {
            LOGGER.error("Error to update product limit for account {}.", account.toString());
            throw e;
        }
    }

    public void sendError(String errorMessage, String productId, Account account, String title) {
        try {
            LOGGER.info("SendError {}, productId:{}, account:{}", errorMessage, productId, account.toString());
            errorMessage = URLEncoder.encode(errorMessage, StandardCharsets.UTF_8.toString());
            title = URLEncoder.encode(title, StandardCharsets.UTF_8.toString());
            String sendErrorUrl = ERROR_URL + EMAIL_FIRST_PARAM + account.getEmail() + KEY_PARAM + account.getKey()
                    + PRODUCT_ID_PARAM + productId + DETAIL_PARAM + errorMessage + TITLE_PARAM + title;

            HttpPost post = new HttpPost(sendErrorUrl);
            client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode != HttpStatus.SC_OK) {
                LOGGER.error("Error when send Error account {}, statusline:{}, statuscode:{}", account.toString(), response.getStatusLine().getStatusCode());
            }
        } catch (Exception e) {
            LOGGER.error("Exception when send error to server", e.fillInStackTrace());
        }
    }

    public void sendSucess(String productId, String amazonKey, String status, Account account) {
        try {
            status = URLEncoder.encode(status, StandardCharsets.UTF_8.toString());
            String sendSuccessUrl = SUCCESS_URL + STATUS_FIRST_PARAM + status + KEY_PARAM + account.getKey() + AMAZON_KEY_PARAM + amazonKey
                    + PRODUCT_ID_PARAM + productId;

            LOGGER.info("Send success url {} account:{}", sendSuccessUrl, account.toString());
            HttpPost post = new HttpPost(sendSuccessUrl);
            client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode != HttpStatus.SC_OK) {
                LOGGER.error("Error when send sendSucess account {}, statusline:{}, statuscode:{}", account.toString(), response.getStatusLine().getStatusCode());
            }
        } catch (Exception e) {
            LOGGER.error("Exception when send success to server", e.fillInStackTrace());
        }
    }

    public void postProductSaleSumary(Account account, List<Summary> summaries) {
        HttpPost post = new HttpPost(POST_PRODUCT_SUMMARY_URL);
        Gson gson = new Gson();
        try {
            SummaryData summaryData = new SummaryData(summaries);
            StringEntity entity = new StringEntity(gson.toJson(summaryData));
            post.setHeader("key", account.getKey());
            post.setHeader("Accept", "application/json");
            post.setHeader("Content-type", "application/json");
            post.setEntity(entity);
            LOGGER.info("post data result of get sale account:{}, sale data: {}", account.toString(), summaryData.toString());
            client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode != HttpStatus.SC_OK) {
                LOGGER.error("Send getSale for account {} fail, statusline:{}, statuscode:{}", account.toString(), response.getStatusLine().getStatusCode());
            } else {
                LOGGER.info("Send getSale success");
            }
        } catch (Exception e) {
            LOGGER.error("Exception when send success to server", e.fillInStackTrace());
        }
    }

    public void postProductAsin(Account account, List<AsinProduct> asinProducts) {
        HttpPost post = new HttpPost(POST_PRODUCT_ASIN_INFO_URL);
        Gson gson = new Gson();
        try {
            AsinProductData data = new AsinProductData(asinProducts);
            StringEntity entity = new StringEntity(gson.toJson(data));
            post.setHeader("key", account.getKey());
            post.setHeader("Accept", "application/json");
            post.setHeader("Content-type", "application/json");
            post.setEntity(entity);
            LOGGER.info("Send product asin for account: {} data: {}", account.toString(), data.toString());
            client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode != HttpStatus.SC_OK) {
                LOGGER.error("Send product asin fail. account {}, statusline:{}, statuscode:{}", account.toString(), response.getStatusLine().getStatusCode());
            } else {
                LOGGER.info("Send product asin Success");
            }
        } catch (Exception e) {
            LOGGER.error("Exception when send success to server", e.fillInStackTrace());
        }
    }

}
