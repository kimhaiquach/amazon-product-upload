package com.amazon.productupload.driver;

import com.amazon.productupload.model.Account;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;

import io.github.bonigarcia.wdm.ChromeDriverManager;

import static com.amazon.productupload.constant.DataContants.PROXY_REFIX;

public class DriverFactory {
    private static final DriverFactory instance = new DriverFactory();

    private DriverFactory() {
    }

    public static DriverFactory getInstance() {
        return instance;
    }

    // Get a new WebDriver Instance.
    // There are various implementations for this depending on browser. The required browser can be set as an environment variable.
    // Refer http://getgauge.io/documentation/user/current/managing_environments/README.html
    public WebDriver getDriver(Account account, String chromeVersion) {
        ChromeDriverManager.chromedriver()
                .version(chromeVersion)
                .setup();
        File canvas = null;
        File rtc = null;
        try {
            String canvasUrl = new File(DriverFactory.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile() + "/Canvas-Defender_v1.1.0.crx";
            canvas = new File(canvasUrl);
            String rtcUrl = new File(DriverFactory.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile() + "/WebRTC-Control_v0.1.9.crx";
            rtc = new File(rtcUrl);
            ChromeOptions chromeOptions = new ChromeOptions().addArguments(PROXY_REFIX + account.getProxy()).
                    addExtensions(canvas, rtc);
            return new ChromeDriver(chromeOptions);
        } catch (Exception e) {
            ChromeOptions chromeOptions = new ChromeOptions().addArguments(PROXY_REFIX + account.getProxy());
            return new ChromeDriver(chromeOptions);
        }
    }
}