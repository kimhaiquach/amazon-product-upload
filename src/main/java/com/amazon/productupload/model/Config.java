package com.amazon.productupload.model;

public class Config {
    private String limit;
    private String productLive;
    private String tier;

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getProductLive() {
        return productLive;
    }

    public void setProductLive(String productLive) {
        this.productLive = productLive;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    @Override
    public String toString() {
        return "Config{" +
                "limit='" + limit + '\'' +
                ", productLive='" + productLive + '\'' +
                ", tier='" + tier + '\'' +
                '}';
    }
}
