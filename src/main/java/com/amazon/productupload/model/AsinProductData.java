package com.amazon.productupload.model;

import java.util.List;

public class AsinProductData {
    private List<AsinProduct> data;

    public AsinProductData(List<AsinProduct> asinProducts) {
        data = asinProducts;
    }

    public List<AsinProduct> getData() {
        return data;
    }

    public void setData(List<AsinProduct> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "AsinProductData{" +
                "data=" + data +
                '}';
    }
}
