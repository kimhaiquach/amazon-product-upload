package com.amazon.productupload.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {
    private String id;
    private String price;
    private int productype;
    private String color;
    private String url_image;
    private String brandname;
    private String title;
    private String key_1;
    private String key_2;
    private String description;
    private String merch_amazon_key;
    private String user_id;
    private boolean is_men;
    private boolean is_women;
    private boolean is_youth;
    private boolean is_upload;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getProductype() {
        return productype;
    }

    public void setProductype(int productype) {
        this.productype = productype;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey_1() {
        return key_1;
    }

    public void setKey_1(String key_1) {
        this.key_1 = key_1;
    }

    public String getKey_2() {
        return key_2;
    }

    public void setKey_2(String key_2) {
        this.key_2 = key_2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMerch_amazon_key() {
        return merch_amazon_key;
    }

    public void setMerch_amazon_key(String merch_amazon_key) {
        this.merch_amazon_key = merch_amazon_key;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public boolean is_men() {
        return is_men;
    }

    public void setIs_men(boolean is_men) {
        this.is_men = is_men;
    }

    public boolean is_women() {
        return is_women;
    }

    public void setIs_women(boolean is_women) {
        this.is_women = is_women;
    }

    public boolean is_youth() {
        return is_youth;
    }

    public void setIs_youth(boolean is_youth) {
        this.is_youth = is_youth;
    }

    public boolean isUpload() {
        return is_upload;
    }

    public void setIsUpload(boolean isUpload) {
        this.is_upload = isUpload;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", price='" + price + '\'' +
                ", productype=" + productype +
                ", color='" + color + '\'' +
                ", url_image='" + url_image + '\'' +
                ", brandname='" + brandname + '\'' +
                ", title='" + title + '\'' +
                ", key_1='" + key_1 + '\'' +
                ", key_2='" + key_2 + '\'' +
                ", description='" + description + '\'' +
                ", merch_amazon_key='" + merch_amazon_key + '\'' +
                ", user_id='" + user_id + '\'' +
                ", is_men=" + is_men +
                ", is_women=" + is_women +
                ", is_youth=" + is_youth +
                ", is_upload=" + is_upload +
                '}';
    }
}
