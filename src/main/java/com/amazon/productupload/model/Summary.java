package com.amazon.productupload.model;

import java.time.LocalDate;

public class Summary {
    private String email;
    private String asin;
    private int purcharsed;
    private int cancelled;
    private int returned;
    private float revenue;
    private float royalties;
    private LocalDate from;
    private LocalDate to;

    public Summary(String email, String asin, int purcharsed, int cancelled, int returned, Float revenue, Float royalties) {
        this.email = email;
        this.asin = asin;
        this.purcharsed = purcharsed;
        this.cancelled = cancelled;
        this.returned = returned;
        this.revenue = revenue;
        this.royalties = royalties;
        this.from = LocalDate.now().minusDays(1);
        this.to = LocalDate.now();
    }

    @Override
    public String toString() {
        return "Summary{" +
                "email='" + email + '\'' +
                ", asin='" + asin + '\'' +
                ", purcharsed=" + purcharsed +
                ", cancelled=" + cancelled +
                ", returned=" + returned +
                ", revenue=" + revenue +
                ", royalties=" + royalties +
                ", from=" + from +
                ", to=" + to +
                '}';
    }
}
