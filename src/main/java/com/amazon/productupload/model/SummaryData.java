package com.amazon.productupload.model;

import java.util.List;

public class SummaryData {
    private List<Summary> data;

    public SummaryData(List<Summary> summaries) {
        data = summaries;
    }

    public List<Summary> getData() {
        return data;
    }

    public void setData(List<Summary> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SummaryData{" +
                "data=" + data +
                '}';
    }
}
