package com.amazon.productupload.model;

public class AsinProduct {
    private String id;
    private String asin;

    public AsinProduct(String id, String asin) {
        this.id = id;
        this.asin = asin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAsin() {
        return asin;
    }

    public void setAsin(String asin) {
        this.asin = asin;
    }

    @Override
    public String toString() {
        return "AsinProduct{" +
                "id='" + id + '\'' +
                ", asin='" + asin + '\'' +
                '}';
    }
}
