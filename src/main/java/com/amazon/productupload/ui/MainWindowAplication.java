package com.amazon.productupload.ui;

import com.amazon.productupload.model.Account;
import com.amazon.productupload.service.AmazonUploadService;
import com.amazon.productupload.util.FileUtil;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import static com.amazon.productupload.constant.DataContants.*;

public class MainWindowAplication extends JFrame {
    public static final int TIME_BETWEEN_MULTIPLE_CLICK = 6000; //6s

    private static final String TITLE = "Amazon product upload";
    private static final String IMPORT = "Import Account";
    private static final String GET_SALE = "Get Sale";
    private static final String GET_PRODUCT = "Get Product";
    private static final String RUN_UPLOAD = "Upload";
    private static final String CHECK_ACCOUNT = "Check Account";
    private static final String NOTIFICATION = "NOTIFICATION";
    private static final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    private static FileUtil fileUtil = FileUtil.getInstance();
    private static JButton uploadBtn;
    private static JButton getProductBtn;
    private static JButton getSummaryBtn;
    private static JButton importAccountBtn;
    private static JButton checkAccountBtn;
    private static JPanel southPanel;
    Map<String, Account> accountMap = new HashMap<>();
    private JMenuBar northMenuBar;
    private JListAccounts jListAccounts;
    private AmazonUploadService uploadService = AmazonUploadService.getInstance();

    public MainWindowAplication() {
        super(TITLE);
        setSize(800, 800);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                int confirmed = JOptionPane.showConfirmDialog(null,
                        "Are you sure you want to exit the program?", "Exit Program",
                        JOptionPane.OK_CANCEL_OPTION);
                if (confirmed == JOptionPane.YES_OPTION) {
                    fileUtil.storeHistorySelectedAccount(jListAccounts.getUnSeletectAccountsModel(),
                            jListAccounts.getSeletectedAccountsModel(), accountMap);
                    dispose();
                }
            }
        });
        setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        initWindows();

        //set Layout for window
        getContentPane().add(BorderLayout.NORTH, northMenuBar);
        getContentPane().add(BorderLayout.CENTER, jListAccounts);
        getContentPane().add(BorderLayout.SOUTH, southPanel);
        setVisible(true);
    }

    private void initWindows() {
        jListAccounts = new JListAccounts();
        initAccounts();
        northMenuBar = initTopWindow();
        southPanel = initBottomWindow();
    }

    private void initAccounts() {
        try {
            String cacheFileUrl = fileUtil.getParentUrl(FileUtil.getInstance()) + ACCOUNT_CACHE_FILE;
            accountMap = new HashMap<>();
            accountMap = fileUtil.readAccountsFromCSV(cacheFileUrl, true);
            jListAccounts.getUnSeletectAccountsModel().clear();
            jListAccounts.getSeletectedAccountsModel().clear();
            for (Map.Entry<String, Account> entry : accountMap.entrySet()) {
                if (entry.getValue().isSelected()) {
                    jListAccounts.getSeletectedAccountsModel().addElement(entry.getValue().getLabel());
                } else {
                    jListAccounts.getUnSeletectAccountsModel().addElement(entry.getValue().getLabel());
                }
            }
        } catch (Exception e) {
            System.out.println("Could not read cache file");
        }
    }

    private JPanel initBottomWindow() {
        JPanel bottom = new JPanel();

        uploadBtn = new JButton(RUN_UPLOAD);
        uploadBtn.setFont(FONT_BOLD_DEFAULT);

        getSummaryBtn = new JButton(GET_SALE);
        getSummaryBtn.setFont(FONT_BOLD_DEFAULT);

        getProductBtn = new JButton(GET_PRODUCT);
        getProductBtn.setFont(FONT_BOLD_DEFAULT);
        checkAccountBtn = new JButton(CHECK_ACCOUNT);
        checkAccountBtn.setFont(FONT_BOLD_DEFAULT);
        bottom.add(uploadBtn);
        bottom.add(checkAccountBtn);
        bottom.add(getProductBtn);
        bottom.add(getSummaryBtn);
        setupListener(bottom);

        return bottom;
    }

    private JMenuBar initTopWindow() {
        northMenuBar = new JMenuBar();
        importAccountBtn = new JButton(IMPORT);
        importAccountBtn.setFont(FONT_LEFT_TO_RIGHT_DEFAULT);
        importAccountBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
                fileChooser.setAcceptAllFileFilterUsed(false);
                FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV FILE", "csv");
                fileChooser.setFileFilter(filter);
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    String fileName = file.getAbsolutePath();
                    try {
                        accountMap = new HashMap<>();
                        accountMap = fileUtil.readAccountsFromCSV(fileName, false);
                        jListAccounts.getUnSeletectAccountsModel().clear();
                        jListAccounts.getSeletectedAccountsModel().clear();
                        for (Map.Entry<String, Account> entry : accountMap.entrySet()) {
                            jListAccounts.getUnSeletectAccountsModel().addElement(entry.getValue().getLabel());
                        }
                    } catch (Exception ex) {
                        JLabel label = new JLabel(ERROR_CSV_LABEL);
                        label.setFont(new Font("Arial", Font.BOLD, 18));
                        JOptionPane.showMessageDialog(null, label, "ERROR", JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });

        northMenuBar.add(importAccountBtn);

        return northMenuBar;
    }

    private void setupListener(JPanel bottom) {
        uploadBtn.setMultiClickThreshhold(TIME_BETWEEN_MULTIPLE_CLICK);
        uploadBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!jListAccounts.getSeletectedAccountsModel().isEmpty()) {
                    Object[] selectedAccounts = jListAccounts.getSeletectedAccountsModel().toArray();
                    for (Object account : selectedAccounts) {
                        try {
                            uploadService.uploadAllProducsByAccount(accountMap.get(String.valueOf(account)));
                        } catch (Exception ex) {
                            System.out.println("Error when upload product: " + ex.getMessage());
                        }
                    }
                    JLabel label = new JLabel(SUCCESS_UPLOAD_LABEL);
                    label.setFont(new Font("Arial", Font.BOLD, 18));
                    JOptionPane.showMessageDialog(null, label, NOTIFICATION, JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

        checkAccountBtn.setMultiClickThreshhold(TIME_BETWEEN_MULTIPLE_CLICK);
        checkAccountBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!jListAccounts.getSeletectedAccountsModel().isEmpty()) {
                    Object[] selectedAccounts = jListAccounts.getSeletectedAccountsModel().toArray();
                    String currentAccount = "";
                    for (Object account : selectedAccounts) {
                        try {
                            currentAccount = String.valueOf(account);
                            uploadService.checkAccount(accountMap.get(currentAccount));
                            uploadService.exitDriver();
                        } catch (Exception ex) {
                            uploadService.exitDriver();
                            JLabel label = new JLabel(String.format(ERROR_CHECKLOGIN_LABEL, account));
                            label.setFont(new Font("Arial", Font.BOLD, 18));
                            JOptionPane.showMessageDialog(null, label, "ERROR", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    JLabel label = new JLabel(SUCCESS_CHECKLOGIN_LABEL);
                    label.setFont(new Font("Arial", Font.BOLD, 18));
                    JOptionPane.showMessageDialog(null, label, NOTIFICATION, JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

        getSummaryBtn.setMultiClickThreshhold(TIME_BETWEEN_MULTIPLE_CLICK);
        getSummaryBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!jListAccounts.getSeletectedAccountsModel().isEmpty()) {
                    Object[] selectedAccounts = jListAccounts.getSeletectedAccountsModel().toArray();
                    for (Object account : selectedAccounts) {
                        try {
                            uploadService.getSummaryByAccount(accountMap.get(String.valueOf(account)));
                        } catch (Exception ex) {
                            System.out.println(ex.getCause());
                        }
                    }
                    JLabel label = new JLabel(SUCCESS_GET_SALE_LABEL);
                    label.setFont(new Font("Arial", Font.BOLD, 18));
                    JOptionPane.showMessageDialog(null, label, NOTIFICATION, JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

        getProductBtn.setMultiClickThreshhold(TIME_BETWEEN_MULTIPLE_CLICK);
        getProductBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!jListAccounts.getSeletectedAccountsModel().isEmpty()) {
                    Object[] selectedAccounts = jListAccounts.getSeletectedAccountsModel().toArray();
                    for (Object account : selectedAccounts) {
                        try {
                            uploadService.getProductAsinFromAmazonByAccount(accountMap.get(String.valueOf(account)));
                        } catch (Exception ex) {
                            System.out.println(ex.getCause());
                        }
                    }
                    JLabel label = new JLabel(SUCCESS_GET_PRODUCT_LABEL);
                    label.setFont(new Font("Arial", Font.BOLD, 18));
                    JOptionPane.showMessageDialog(null, label, NOTIFICATION, JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
    }
}
