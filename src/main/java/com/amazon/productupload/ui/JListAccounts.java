package com.amazon.productupload.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;
import javax.swing.border.LineBorder;

import static com.amazon.productupload.constant.DataContants.*;

public class JListAccounts extends JPanel {
    private static final int LIST_ROW_COUNT = 24;
    private static final int LIST_CHAR_WIDTH = 55;
    private static final String LIST_PROTOTYPE = "%" + LIST_CHAR_WIDTH + "s";
    private static final String ADD = "   >>   ";
    private static final String REMOVE = "   <<   ";
    private static final String SELECTED_ACCOUNT = "Selected Accounts";
    private static final String AVAILABLE_ACCOUNT = "All available Accounts";

    private static DefaultListModel<String> unSeletectAccountsModel = new DefaultListModel();
    private static DefaultListModel<String> seletectedAccountsModel = new DefaultListModel();
    private JList<String> unSeletectAccounts;
    private JList<String> selectedAccounts;
    private JButton addButton = new JButton(ADD);
    private JButton removeButton = new JButton(REMOVE);
    private JPanel addRemoveWrapper = new JPanel(new GridLayout(2, 1, 20, 10));

    public JListAccounts() {
        setLayout(new BorderLayout());

        unSeletectAccounts = new JList<>(unSeletectAccountsModel);
        unSeletectAccounts.setFixedCellHeight(LIST_ROW_COUNT);
        unSeletectAccounts.setFixedCellWidth(300);
        unSeletectAccounts.setVisibleRowCount(LIST_ROW_COUNT);
        unSeletectAccounts.setFont(FONT_LEFT_TO_RIGHT_DEFAULT);
        unSeletectAccounts.setPrototypeCellValue(String.format(LIST_PROTOTYPE, ""));

        selectedAccounts = new JList<>(seletectedAccountsModel);
        selectedAccounts.setFixedCellHeight(25);
        selectedAccounts.setFixedCellWidth(300);
        selectedAccounts.setVisibleRowCount(LIST_ROW_COUNT);
        selectedAccounts.setFont(FONT_LEFT_TO_RIGHT_DEFAULT);
        selectedAccounts.setPrototypeCellValue(String.format(LIST_PROTOTYPE, ""));

        JScrollPane unSelectScroll = new JScrollPane(unSeletectAccounts);
        JScrollPane selectedScroll = new JScrollPane(selectedAccounts);
        addRemoveWrapper.add(addButton);
        addRemoveWrapper.add(removeButton);

        addButton.setFont(new Font(null, Font.BOLD, 22));
        addButton.setBorderPainted(false);
        addButton.setBorder(new LineBorder(Color.BLACK));
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!unSeletectAccounts.isSelectionEmpty()) {
                    List<String> selectedValues = unSeletectAccounts.getSelectedValuesList();
                    for (String selectedValue : selectedValues) {
                        unSeletectAccountsModel.removeElement(selectedValue);
                        seletectedAccountsModel.addElement(selectedValue);
                    }
                }
            }
        });

        removeButton.setFont(new Font(null, Font.BOLD, 22));
        removeButton.setBorder(new LineBorder(Color.BLACK));
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!selectedAccounts.isSelectionEmpty()) {
                    List<String> selectedValues = selectedAccounts.getSelectedValuesList();
                    for (String selectedValue : selectedValues) {
                        unSeletectAccountsModel.addElement(selectedValue);
                        seletectedAccountsModel.removeElement(selectedValue);
                    }
                }
            }
        });
        JPanel centerContent = new JPanel();
        JPanel wrapLableAndUnselectScroll = new JPanel(new BorderLayout());
        JLabel unselectLable = new JLabel(AVAILABLE_ACCOUNT);
        unselectLable.setVerticalAlignment(JLabel.CENTER);
        unselectLable.setHorizontalAlignment(JLabel.CENTER);
        unselectLable.setFont(FONT_BOLD_DEFAULT_16);
        wrapLableAndUnselectScroll.add(BorderLayout.NORTH, unselectLable);
        wrapLableAndUnselectScroll.add(BorderLayout.CENTER, unSelectScroll);

        JPanel wrapLableAndSelectedScroll = new JPanel(new BorderLayout());
        JLabel selectedLable = new JLabel(SELECTED_ACCOUNT);
        selectedLable.setFont(FONT_BOLD_DEFAULT_16);
        selectedLable.setVerticalAlignment(SwingConstants.CENTER);
        selectedLable.setVerticalAlignment(JLabel.CENTER);
        selectedLable.setHorizontalAlignment(JLabel.CENTER);

        wrapLableAndSelectedScroll.add(BorderLayout.NORTH, selectedLable);
        wrapLableAndSelectedScroll.add(BorderLayout.CENTER, selectedScroll);
        centerContent.add(wrapLableAndUnselectScroll);
        centerContent.add(addRemoveWrapper);
        centerContent.add(wrapLableAndSelectedScroll);
        add(BorderLayout.CENTER, centerContent);
    }


    public DefaultListModel<String> getUnSeletectAccountsModel() {
        return unSeletectAccountsModel;
    }

    public void setUnSeletectAccountsModel(DefaultListModel<String> unSeletectAccountsModel) {
        this.unSeletectAccountsModel = unSeletectAccountsModel;
    }

    public DefaultListModel<String> getSeletectedAccountsModel() {
        return seletectedAccountsModel;
    }

    public void setSeletectedAccountsModel(DefaultListModel<String> seletectedAccountsModel) {
        this.seletectedAccountsModel = seletectedAccountsModel;
    }
}
