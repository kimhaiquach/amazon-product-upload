package com.amazon.productupload.util;

import com.amazon.productupload.model.Account;
import com.amazon.productupload.model.Summary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.*;

import static com.amazon.productupload.constant.DataContants.*;

public class FileUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);
    private static FileUtil instance = new FileUtil();

    private FileUtil() {
    }

    private static Account stringToAccount(String line, boolean isBackupFile) {
        String[] columns = line.split(COMMA);// a CSV has comma separated lines
        if (columns.length != 0) {
            Account account = new Account();
            account.setEmail(columns[0]);//<-- this is the first column in the csv file
            account.setPassword(columns[1]);
            account.setKey(columns[2]);
            account.setReview(columns[3]);
            account.setProxy(columns[4]);
            account.setProfile(columns[5]);
            account.setLabel(columns[6]);
            if (isBackupFile) {
                account.setSelected(Boolean.valueOf(columns[7]));
            }

            //more initialization goes here
            return account;
        }
        return null;
    }

    public static FileUtil getInstance() {
        return instance;
    }

    public Map<String, Account> readAccountsFromCSV(String accountFile, boolean isBackupFile) throws IOException {
        Map<String, Account> result = new LinkedHashMap<>();
        InputStream inputStream;
        if (new File(accountFile).exists()) {
            System.out.println("Readfile from directory" + accountFile);
            File file = new File(accountFile);
            try {
                inputStream = new FileInputStream(file);
                try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
                    if (isBackupFile) {
                        br.lines().forEach(line -> {
                                    Account account = stringToAccount(line, isBackupFile);
                                    if (account != null) {
                                        result.put(account.getLabel(), account);
                                    }
                                }
                        );
                    } else {
                        br.lines().skip(1).forEach(line -> {
                                    Account account = stringToAccount(line, isBackupFile);
                                    if (account != null) {
                                        result.put(account.getLabel(), account);
                                    }
                                }
                        );
                    }
                }

                return result;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public String saveImage(String imageUrl) throws IOException, URISyntaxException {
        URL website = new URL(imageUrl);
        String localImageUrl = getParentUrl(FileUtil.getInstance()) + Calendar.getInstance().getTimeInMillis() + ".png";
        System.out.println("DEBUG infor imageurl before save:" + localImageUrl);
        Path imagePath = new File(localImageUrl).toPath();
        try (InputStream in = website.openStream()) {
            Files.copy(in, imagePath, StandardCopyOption.REPLACE_EXISTING);
        }

        System.out.println("DEBUG infor image url after save:" + imagePath);

        return imagePath.toString();
    }

    public void storeHistorySelectedAccount(DefaultListModel unselectAccounts, DefaultListModel selectedAccounts, Map<String, Account> accountMap) {
        String cacheFileUrl = null;
        try {
            cacheFileUrl = getParentUrl(FileUtil.getInstance()) + ACCOUNT_CACHE_FILE;
            deleteFile(cacheFileUrl);
            FileWriter fileWriter = new FileWriter(cacheFileUrl);
            for (Object selectedAccount : selectedAccounts.toArray()) {
                Account account = accountMap.get(selectedAccount);
                StringBuilder sb = new StringBuilder();
                sb.append(account.getEmail()).
                        append(",").append(account.getPassword()).append(",").append(account.getKey())
                        .append(",").append(account.getReview()).append(",").append(account.getProxy())
                        .append(",").append(account.getProfile()).append(",").append(account.getLabel())
                        .append(",").append("true\n");
                fileWriter.write(sb.toString());
            }

            for (Object unselectAccount : unselectAccounts.toArray()) {
                StringBuilder sb = new StringBuilder();
                Account account = accountMap.get(unselectAccount);
                sb.append(account.getEmail()).
                        append(",").append(account.getPassword()).append(",").append(account.getKey())
                        .append(",").append(account.getReview()).append(",").append(account.getProxy())
                        .append(",").append(account.getProfile()).append(",").append(account.getLabel())
                        .append(",").append("false\n");
                fileWriter.write(sb.toString());
            }
            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public <T> String getParentUrl(T t) throws URISyntaxException {
        return new File(t.getClass().getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile() + "/";
    }

    public void deleteFile(String fileUrl) {
        try {
            Files.delete(Paths.get(fileUrl));
        } catch (Exception e) {
            LOGGER.error("Could not delete image: {}", fileUrl, e);
        }
    }

    public Summary toProductSumary(String email, String productInfo, String asin) {
        String[] productSaleInfo = productInfo.split(" ");
        int size = productSaleInfo.length;
        Float royalties = Float.parseFloat(productSaleInfo[size - 1].substring(1));
        Float revenue = Float.parseFloat(productSaleInfo[size - 2].substring(1));
        int returned = Integer.parseInt(productSaleInfo[size - 3]);
        int cancelled = Integer.parseInt(productSaleInfo[size - 4]);
        int purcharsed = Integer.parseInt(productSaleInfo[size - 5]);
        return new Summary(email, asin, purcharsed, cancelled, returned, revenue, royalties);
    }
}

